### Lition Node - Azure Template

<a href="https://portal.azure.com/#create/Microsoft.Template/uri/https%3A%2F%2Fglcdn.githack.com%2Flition%2Flition-node-azure-template%2Fraw%2Fmaster%2Fazuredeploy.json" target="_blank">
    <img src="https://raw.githubusercontent.com/Azure/azure-quickstart-templates/master/1-CONTRIBUTION-GUIDE/images/deploytoazure.png"/>
</a>
<a href="http://armviz.io/#/?load=https%3A%2F%2Fgitlab.com%2Flition%2Flition-node-azure-template%2Fraw%2Fmaster%2Fazuredeploy.json" target="_blank">
    <img src="https://raw.githubusercontent.com/Azure/azure-quickstart-templates/master/1-CONTRIBUTION-GUIDE/images/visualizebutton.png"/>
</a>

This template allows you to deploy an Ubuntu VM with the Lition Maker tool'. In order to run a Lition Node, you must follow these steps:

1. Use the [Side Chain Manager](https://lition.io/sidechainmanagerv2) to start vesting in a Sidechain.
2. Launch a VM with this template.
3. Connect to the VM and run the following commands:

```
screen
cd lition-maker
sudo ./setup.sh
```

4. Follow the instructions to join a network.

For more information on how to use the Side Chain Manager or the Lition Make tool, you can follow the [Wiki](https://gitlab.com/lition/lition-maker/wikis/Join-Network-as-a-validator), considering that this VM replaces the steps to run Lition Maker locally.
